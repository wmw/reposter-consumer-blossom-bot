#FROM golang:alpine
FROM golang:alpine AS build

WORKDIR /app
COPY ./ /app
#RUN apk add --update --no-cache font-noto-emoji
RUN go mod download && go build

FROM alpine

ENV TZ Europe/Moscow

RUN apk add --update --no-cache tzdata && cp /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /app
COPY --from=build /app/entr /app/rcbt ./
USER 1000:1000
ENTRYPOINT echo "./rcbt" | ./entr -r ./rcbt
