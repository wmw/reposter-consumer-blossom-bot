module rcbt

go 1.14

require (
	github.com/parnurzeal/gorequest v0.2.16
	github.com/pkg/errors v0.9.1 // indirect
	github.com/streadway/amqp v1.0.0
	github.com/tidwall/gjson v1.6.0
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
